package com.android.AndroidColorApp;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
//import java.lang.*;

public class ListOfColorsHeader extends LinearLayout implements OnClickListener{
	private static final String TAG = "ListOfColorsHeader";
	
	private Context rootContext;
	private Button btnShowColor;
	private EditText etxtColorQuery;
	private boolean noSpace = false;
	private boolean isHex = false;
	private boolean moreThanZeroChars = false;
	private Toast toastNotif;
	private AndroidColorAppActivity mParent;
	private Intent colorDisplayScreen;
	
	public ListOfColorsHeader(Context context) {
		super(context);
		rootContext = context;
		initialize();
	}
	
	/*
	 * This is called when a view is being constructed from an XML file, 
	 * supplying attributes that were specified in the XML file. 
	 */
	public ListOfColorsHeader(Context context, AttributeSet attrs) {
		super(context, attrs);
		rootContext = context;
		btnShowColor = new Button(this.getContext());
		etxtColorQuery = new EditText(this.getContext());
		initialize();
	}
	
	public void initialize() {
		this.setOrientation(LinearLayout.HORIZONTAL);		
		
		btnShowColor.setOnClickListener(this);
        btnShowColor.setEnabled(false);
        
        etxtColorQuery.addTextChangedListener(new TextWatcher(){
            public void afterTextChanged(Editable s) {}
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){
            	String tmpS = s.toString();//.replaceAll("\\s+", "");
            	String strErr="No error";
	        	if ((s.length() > 0))
	        	{
	        		moreThanZeroChars=true;
	        		if (tmpS.contains("\\s+"))
	        		{
	        			noSpace=false;
	        			strErr="remove your spaces";
	        		}
	        		else
	        		{
	        			noSpace=true;
	        			int tmpHex = 99999999;
		        		try 
		        		{
		        			tmpHex = Integer.parseInt(tmpS, 16);
		        		}
		        		catch (NumberFormatException nfe)
		        		{
		        			isHex=false;
		        			strErr="Enter a hex value (e.g. \"FFFFFF\")";
		        			//Log.e(TAG,nfe.getMessage());
		        		}
		        		finally
		        		{
		        			if (tmpHex!=99999999)
		        			{
		        				isHex=true;		
		        			}
		        		}
	        		}
	        		
	        	}
	        	else
	        	{
	        		moreThanZeroChars=false;
	        		strErr="A hex value is six characters long";
	        	}
	        	Log.v(TAG,"tmpS ("+noSpace+" "+isHex+" "+moreThanZeroChars+")  = \""+tmpS+"\"");
	        	if (!noSpace||!isHex||!moreThanZeroChars)
	        	{
	        	    if(toastNotif == null)
	        	    {   
	        	    	toastNotif = Toast.makeText(rootContext, strErr, Toast.LENGTH_SHORT);
	        	    }

	        	    toastNotif.setText(strErr);
	        	    toastNotif.setDuration(Toast.LENGTH_LONG);
	        	    toastNotif.show();
	        	}
        		btnShowColor.setEnabled(noSpace&&isHex&&moreThanZeroChars); 
            }
        });
		        
        etxtColorQuery.setHint(R.string.etxtHint);
        etxtColorQuery.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,1));
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(6);
        etxtColorQuery.setFilters(FilterArray);
        this.addView(etxtColorQuery);
		
		
		btnShowColor.setText(R.string.btnText);
		this.addView(btnShowColor);
	}

	void setParent(AndroidColorAppActivity par)
	{
		mParent = par;
	}
	
	@Override
	public void onClick(View v) {
		colorDisplayScreen = new Intent(mParent, ColorDisplayerScreen.class);
		//TODO: optimize later
		/*int tmp = Integer.parseInt(etxtColorQuery.getText().toString(),16);
		while (tmp < 0x100000)
		{
			tmp = tmp << 4;
		}
		colorDisplayScreen.putExtra("colorToDisplay",tmp|0xFF000000);*/
		String tmp = etxtColorQuery.getText().toString().toUpperCase();
		while (tmp.length() < 6)
		{
			tmp+="0";
		}
		colorDisplayScreen.putExtra("colorToDisplay",tmp);
		mParent.startActivity(colorDisplayScreen);
	}
}
