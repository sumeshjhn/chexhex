package com.android.AndroidColorApp;

import android.app.Activity;
import android.os.Bundle;
//import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.TextView;

public class ColorDisplayerScreen extends Activity implements OnClickListener {
	private static final String TAG = "Color Displayer Screen :: ";
	private Bundle colorHashMap;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        // hide titlebar of application
        // must be before setting the layout
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.colordisplayerscreen);    
        FrameLayout fraLay = (FrameLayout) this.findViewById(R.id.rootCDSLay);
        TextView txtvColorLabel = (TextView) this.findViewById(R.id.textView1);
        fraLay.setOnClickListener(this);
        colorHashMap = this.getIntent().getExtras();
        if (colorHashMap!=null)
        {
        	String strColorOfInterest = colorHashMap.getString("colorToDisplay");
        	txtvColorLabel.setText("#"+strColorOfInterest);
        	txtvColorLabel.setPadding(30, 0, 0, 0);
        	txtvColorLabel.setTextSize(40);
        	//Log.v(TAG,"Color to print = "+String.format("%x", Integer.parseInt(strColorOfInterest, 16)|0xFF000000));        	
        	fraLay.setBackgroundColor(Integer.parseInt(strColorOfInterest, 16)|0xFF000000);
            txtvColorLabel.setTextColor(((Integer.parseInt(strColorOfInterest, 16)|0xFF000000)-0x00005000));
        	fraLay.invalidate();
        	        	 
        	/*us this if optimized
        	 * int intColorOfInterest = colorHashMap.getInt("colorToDisplay");
        	txtvColorLabel.setText("#"+String.format("%X", intColorOfInterest));
        	txtvColorLabel.setPadding(30, 0, 0, 0);
        	txtvColorLabel.setTextSize(40);
        	Log.v(TAG,"Color to print = "+String.format("%x", intColorOfInterest));        	
        	fraLay.setBackgroundColor(intColorOfInterest);
            txtvColorLabel.setTextColor(intColorOfInterest-0x00005000);
        	fraLay.invalidate();*/
        }        
    }
	
	@Override
	public void onClick(View v) {
		//Log.v(TAG,"note...if white, UCK");
		setResult(RESULT_OK, this.getIntent());
        finish();

	}

}
