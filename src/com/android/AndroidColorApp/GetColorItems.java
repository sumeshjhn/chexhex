package com.android.AndroidColorApp;
import java.util.ArrayList;


public class GetColorItems implements Runnable {

	private ArrayList<ColorModel> myListItems;
	private int itemsPerPage = 0;
	private ListOfColorsArrayAdapter locAA;
	private AndroidColorAppActivity mParentThread;	
	
	public GetColorItems (ArrayList<ColorModel> aLCM, ListOfColorsArrayAdapter l, int numOfItems, AndroidColorAppActivity thread)
	{		
		this.myListItems = aLCM;
		this.itemsPerPage = numOfItems;
		this.locAA = l;
		this.mParentThread = thread;
	}
	@Override
	public void run() {
		//Set flag so we cant load new items 2 at the same time		
		mParentThread.setLoading(true);
		
		//Reset the array that holds the new items
    	myListItems = new ArrayList<ColorModel>();
    	
		//Simulate a delay, delete this on a production environment!
    	/*try { Thread.sleep(3000);
		} catch (InterruptedException e) {}*/
		
		//Get 7 new listitems
    	for (int i = 0; i < itemsPerPage; i++) {		
        	//myListItems.add(new ColorModel("000A00"));
    		myListItems.add(new ColorModel(ColorModel.createRandomString(6)));
		}
		
		//Done! now continue on the UI thread
    	this.mParentThread.runOnUiThread(new UpdateUIThread(this.myListItems, this.locAA, this.mParentThread));
        
	}

}
