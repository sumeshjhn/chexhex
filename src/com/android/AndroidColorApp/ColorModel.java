package com.android.AndroidColorApp;

import java.util.Random;

//import android.util.Log;

public class ColorModel {
	private String mHexColor;
	private final static int MAX_HEX = 16777215;
	private final static int MIN_HEX = 0;
	
	public ColorModel(String hex)
	{
		this.mHexColor=hex;
	}
	
	public int getHexColorIntWithAlpha()
	{
		return (Integer.parseInt(this.mHexColor,16))|0xFF000000;
	}
	
	public String getHexColorStr(boolean format)
	{
		return (format) ? "#"+mHexColor : mHexColor;
	}

	public static String createRandomString(int length) {
		Random random = new Random();
		StringBuilder sb = new StringBuilder();		
		//String tmp="";		
		while (sb.length() < length) {
		//while (tmp.length() < length) {
			sb.append(Integer.toHexString(random.nextInt(MAX_HEX-MIN_HEX)+MIN_HEX));
			//tmp+=Integer.toHexString(random.nextInt(MAX_HEX-MIN_HEX)+MIN_HEX);
		}		
		String retStr = sb.toString().toUpperCase();
		//Log.e("Error",retStr+" "+sb.length());
		return (sb.length() > 6)?retStr.substring(3,8):retStr;
	}
}
