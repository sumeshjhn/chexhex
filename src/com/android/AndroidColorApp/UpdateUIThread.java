package com.android.AndroidColorApp;

import java.util.ArrayList;

public class UpdateUIThread implements Runnable {

	private ArrayList<ColorModel> myListItems;
	private ListOfColorsArrayAdapter locAA;
	private AndroidColorAppActivity mParentParentThread;
	
	public UpdateUIThread(ArrayList<ColorModel> ali, ListOfColorsArrayAdapter l, AndroidColorAppActivity t) {
		this.myListItems = ali;
		this.locAA = l;
		this.mParentParentThread = t;
	}

	@Override
    public void run() {
    	
		//Loop thru the new items and add them to the adapter
		if(myListItems != null && myListItems.size() > 0){
            for(int i=0;i<myListItems.size();i++)
            {	
            	locAA.add(myListItems.get(i));
            	//Log.i(TAG, "Hexvalue: "+ColorModel.createRandomString(6));
            }                	
        }
    	
		//Tell to the adapter that changes have been made, this will cause the list to refresh
    	locAA.notifyDataSetChanged();
        
    	mParentParentThread.setLoading(false);
    	//mParentParentThread.dismissDialog();
		//Done loading more.
    }

}
