package com.android.AndroidColorApp;

import java.util.ArrayList;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
//import android.util.Log;
//KEEPTHIS: import android.view.LayoutInflater;
import android.view.View;
//KEEPTHIS: import android.view.ViewGroup;
//KEEPTHIS: import android.view.Window;
import android.widget.AdapterView;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
//KEEPTHIS: import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AndroidColorAppActivity extends ListActivity implements OnItemClickListener, OnScrollListener {
	
	private static final String TAG = "AndroidColorAppActivity";	
	private ListOfColorsHeader mListHeader;
	private Toast toastNotif;
	private Intent colorDisplayScreen;
	/*final ColorModel[] arrayOfColors = {new ColorModel(0xFFFFF0),
										new ColorModel(0x8585A1), 
										new ColorModel(0x657432), 
										new ColorModel(0xFF0000), 
										new ColorModel(0x990000), 
										new ColorModel(0x3333CC), 
										new ColorModel(0x7BCC70)};*/
	int itemsPerPage = 7;
	boolean loadingMore = false;
	
	ArrayList<ColorModel> myListItems;
	ListOfColorsArrayAdapter locAA;
	private ProgressDialog pd;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        // hide titlebar of application
        // must be before setting the layout
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        myListItems = new ArrayList<ColorModel>();
        /*myListItems.add(new ColorModel("FFFFF0"));
        myListItems.add(new ColorModel("8585A1"));
		myListItems.add(new ColorModel("657432"));
		myListItems.add(new ColorModel("FF0000"));
		myListItems.add(new ColorModel("990000"));
		myListItems.add(new ColorModel("3333CC"));
		myListItems.add(new ColorModel("7BCC70"));*/
        ListView forTheList = (ListView) this.findViewById(android.R.id.list);
        locAA = new ListOfColorsArrayAdapter(this, myListItems);
        forTheList.setAdapter(locAA);
        forTheList.setOnItemClickListener(this);
        forTheList.setOnScrollListener(this);
        mListHeader = (ListOfColorsHeader) this.findViewById(R.id.queryHeader);
        mListHeader.setParent(this);

        //Load the first 7 items...but commented out because scroll listener gets called anyway
        //threadStartWithDialog();
    }

    public boolean isLoading()
    {
    	return this.loadingMore;
    }
    
    public void setLoading(boolean ld)
    {
    	this.loadingMore = ld;
    }
    private void threadStartWithDialog()
    {
    	//Log.e(TAG,"called this");
    	//this.pd = ProgressDialog.show(this, "Working..", "Dispaying Colors", true, true);
		Thread thread =  new Thread(null, new GetColorItems(myListItems, locAA, itemsPerPage, this));				
		thread.start();
    }
    
    public void dismissDialog()
    {
    	//this.loadingMore = false;
    	handler.sendEmptyMessage(0);
    }
    
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        		//Log.e(TAG,"dismiss dialog");
                pd.dismiss();
        }
    };
    
	@Override
	public void onItemClick(AdapterView<?> parent, View view,
	        int position, long id) {
		colorDisplayScreen = new Intent(this, ColorDisplayerScreen.class);
		
		String tmp = locAA.getItem(position).getHexColorStr(false);
		colorDisplayScreen.putExtra("colorToDisplay",tmp);
		this.startActivity(colorDisplayScreen);
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		
			int lastInScreen = firstVisibleItem + visibleItemCount;
			//Log.i(TAG,"loadingMore="+loadingMore+"\nlastInScreen="+lastInScreen+"\ntotalItemCount="+totalItemCount+"\nitemsPerPage*8="+itemsPerPage*8);
			if ((!this.loadingMore)&&(lastInScreen==totalItemCount)&&(totalItemCount<itemsPerPage*8))
			{
				threadStartWithDialog();
			}
			
			if (totalItemCount>=itemsPerPage*8)
			{
        	    if(toastNotif == null)
        	    {   
        	    	toastNotif = Toast.makeText(getApplicationContext(), "Try using the search bar for more colours!", Toast.LENGTH_SHORT);
        	    }
        	    toastNotif.setDuration(Toast.LENGTH_SHORT);
        	    toastNotif.show();
			}
				
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// do nothing here		
	}
}