package com.android.AndroidColorApp;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ListOfColorsArrayAdapter extends ArrayAdapter<ColorModel> {

	private final Context context;
	private ArrayList<ColorModel> values;

	public ListOfColorsArrayAdapter(Context context, ArrayList<ColorModel> values) {
		super(context, R.layout.thelistofcolors, values);
		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.thelistofcolors, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.label);
		textView.setText(values.get(position).getHexColorStr(true));
		textView.setTextColor(values.get(position).getHexColorIntWithAlpha());
		return rowView;
	}
}
